package github

import (
	"errors"
	"fmt"
	"os"
)

func getToken(envVarName string) (string, error) {
	token, isSet := os.LookupEnv(envVarName)
	if !isSet {
		return "", errors.New(fmt.Sprintf("Environment variable '%s' is not set.", envVarName))
	}

	return token, nil
}