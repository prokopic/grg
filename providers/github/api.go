package github

import (
	"fmt"
)

const (
	ApiUrlBase = "https://api.github.com"
)

func getRepoUrl(username string, repo string) string {
	return fmt.Sprintf("%s/repos/%s/%s", ApiUrlBase, username, repo)
}
