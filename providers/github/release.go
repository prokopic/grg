package github

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"strings"

	"gitlab.com/prokopic/grg/download"
)

type Release struct {
	ID      *int           `json:id`
	Assets  []ReleaseAsset `json:"assets"`
	TagName *string        `json:"tag_name"`
}

type ReleaseAsset struct {
	BrowserDownloadURL *string `json:"browser_download_url,omitempty"`
	Name               string  `json:"name"`
	URL                *string `json:"url"`
}

type ReleaseDownloadError struct {
	Message          *string `json:"message"`
	DocumentationUrl *string `json:"documentation_url"`
}

func DownloadLatest(username string, repo string, namePattern string, tokenEnvVar string) ([]string, error) {
	token, err := getToken(tokenEnvVar)
	if err != nil {
		return nil, err
	}

	// URL for getting data about the latest release
	repoUrl := getRepoUrl(username, repo)
	latestReleaseUrl := fmt.Sprintf("%s/%s/%s", repoUrl, "releases", "latest")

	// HTTP request
	request, err := http.NewRequest(http.MethodGet, latestReleaseUrl, nil)
	if err != nil {
		return nil, err
	}

	// Set Authentication token
	header := map[string]string{
		"Authorization": fmt.Sprintf("token %s", token),
	}
	for key, val := range header {
		request.Header.Set(key, val)
	}

	// Submit request
	client := http.DefaultClient
	fmt.Printf("[inf] Downloading the latest release from '%s'.\n", latestReleaseUrl)
	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	if response.StatusCode == http.StatusNotFound {
		return nil, errors.New(
			fmt.Sprintf(
				"[err] Repository %s/%s does not exist or you do not have sufficient permissions to access it.",
				username,
				repo))
	}

	// Get raw JSON response
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	// fmt.Println(string(body))
	var latestRelease Release
	err = json.Unmarshal(body, &latestRelease)
	if err != nil {
		return nil, err
	}

	var downloadedAssets []string

	for _, asset := range latestRelease.Assets {
		if !strings.Contains(asset.Name, namePattern) {
			continue
		}

		if asset.URL != nil {
			fmt.Printf("[dbg] Downloading '%s' from '%s'.\n", asset.Name, *asset.URL)
			header["Accept"] = "application/octet-stream"
			assetDownloadPath := filepath.Join("/tmp", asset.Name)
			err = download.File(*asset.URL, header, assetDownloadPath)
			if err != nil {
				return nil, err
			}
			downloadedAssets = append(downloadedAssets, assetDownloadPath)
		}
	}

	return downloadedAssets, nil
}
