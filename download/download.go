package download

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

func File(url string, requestHeaders map[string]string, destination string) error {
	var response *http.Response
	var err error

	if requestHeaders == nil {
		response, err = http.Get(url)
	} else {
		request, err := http.NewRequest(http.MethodGet, url, nil)
		if err != nil {
			return err
		}
		for key, val := range requestHeaders {
			request.Header.Set(key, val)
		}
		client := http.DefaultClient
		response, err = client.Do(request)
	}

	if err != nil {
		return err
	}
	defer response.Body.Close()

	destinationFile, err := os.Create(destination)
	if err != nil {
		return err
	}
	defer destinationFile.Close()

	_, err = io.Copy(destinationFile, response.Body)
	if err == nil {
		fmt.Println("[dbg] Download completed.")
	}
	return err
}
