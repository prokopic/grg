package extract

import (
	"archive/tar"
	"compress/gzip"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

func Extract(archivePath string, itemsToExtract []string, destination string) error {
	fmt.Printf("[dbg] Extracting archive '%s'.\nItems:\n", archivePath)
	for _, item := range itemsToExtract {
		fmt.Printf("    - %s\n", item)
	}

	archive, err := os.Open(archivePath)
	if err != nil {
		return err
	}

	// TODO: handle error from Close()
	defer archive.Close()

	gzr, err := gzip.NewReader(archive)
	if err != nil {
		return err
	}

	// TODO: handle error from Close()
	defer gzr.Close()
	tarReader := tar.NewReader(gzr)

	// TODO: add max file count to prevent infinite loop in case of a reader bug
	for {
		header, err := tarReader.Next()
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}

		// see https://gist.github.com/sdomino/635a5ed4f32c93aad131#file-untargz-go-L26
		if header == nil {
			continue
		}

		filename := header.FileInfo().Name()

		if contains(itemsToExtract, filename) {
			destinationFullPath := filepath.Join(destination, filename)

			switch header.Typeflag {

			// TODO: implement directory extraction
			case tar.TypeDir:
				return errors.New("directory extraction is not currently implemented")

			// Extracting file
			case tar.TypeReg:
				fmt.Printf("[dbg] Extracting '%s'...\n", filename)
				destinationFile, err := os.OpenFile(destinationFullPath, os.O_CREATE|os.O_RDWR, os.FileMode(header.Mode))
				if err != nil {
					return err
				}

				// extract file
				_, err = io.Copy(destinationFile, tarReader)
				if err != nil {
					return err
				}

				// TODO: check returned error
				_ = destinationFile.Close()
			}
		}
	}

	fmt.Println("[dbg] Extraction completed.")
	return nil
}

func contains(items []string, s string) bool {
	for _, item := range items {
		if item == s {
			return true
		}
	}

	return false
}
