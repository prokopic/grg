/*
Copyright © 2020 Prokopić Engineering <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"

	"gitlab.com/prokopic/grg/extract"
	"gitlab.com/prokopic/grg/providers/github"
)

// installCmd represents the install command
var installCmd = &cobra.Command{
	Use:   "install",
	Short: "Installs a release from the specified repository",
	Long: `Installs a release from the specified repository.
Only Linux releases and GitHub repositories are
currently supported.

Example usage:
  grg install digitalocean/doctl -d ~/bin -a GITHUB_TOKEN_VAR_NAME`,
	Args: cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		for _, arg := range args {
			err := install(
				arg,
				cmd.Flag("install-dir").Value.String(),
				cmd.Flag("auth-token-env-var").Value.String())
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(installCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// installCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// installCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

	// Installation directory
	homeDir, err := os.UserHomeDir()
	if err != nil {
		homeDir = "~"
	}
	defaultInstallDir := filepath.Join(homeDir, "grg")
	installCmd.Flags().StringP(
		"install-dir",
		"d",
		defaultInstallDir,
		"Installation directory")

	// Auth token environment variable
	installCmd.Flags().StringP(
		"auth-token-env-var",
		"a",
		"GITHUB_API_TOKEN",
		"GitHub API token environment variable")
}

func install(repository string, installDir string, tokenEnvVar string) error {
	fmt.Printf("[inf] Installing latest release from repository %s.\n", repository)
	parts := strings.Split(repository, "/")
	if len(parts) != 2 {
		return errors.New("repository name must be in format 'owner/repo_name'")
	}
	owner := parts[0]
	repositoryName := parts[1]

	// download archive
	archives, err := github.DownloadLatest(owner, repositoryName, "linux", tokenEnvVar)
	if err != nil {
		return err
	}

	// set up installation directory

	// create it if it doesn't exist
	if _, err = os.Stat(installDir); os.IsNotExist(err) {
		err = os.MkdirAll(installDir, 0755)
		if err != nil {
			return err
		}
	}

	// extract assets from archives
	for _, archive := range archives {
		err = extract.Extract(archive, []string{repositoryName}, installDir)
		if err != nil {
			return err
		}
	}

	fmt.Printf("[inf] %s successfully installed to %s.\n", repository, installDir)
	return nil
}
